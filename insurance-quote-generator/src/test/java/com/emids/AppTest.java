package com.emids;

import com.emids.entity.Person;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.junit.runner.RunWith;
import static org.junit.Assert.*;

/**
 * Unit test for simple App.
 */
@RunWith(SpringRunner.class)
//@WebMvcTest(value = UserController.class, secure = false)
@SpringBootTest
public class AppTest

    //extends TestCase
{
    @Autowired
    QuoteController controller;
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
  //  public AppTest( String testName )
   /* {
        super( testName );
    }*/

    /**
     * @return the suite of tests being tested
     */
   // public static Test suite()
  /*  {
        return new TestSuite( AppTest.class );
    }
*/
    /**
     * Rigourous Test :-)
     */
    @org.junit.Test
    public void testApp()
    {
        Person person1 = new Person("Norman","Gomes", 34, 'M', false,
                false, false, true, false, true,
                true, false);

       // = new QuoteController();
        assertEquals(controller.generateInsuranceQuote(person1), "Health Insurance Premium for Mr.Gomes 6850" );

    }

    @org.junit.Test
    public void testNotEquals() {
        Person person2 = new Person("Norman","Gomes", 34, 'M', false,
                false, false, true, false, true,
                true, false);
        assertNotEquals(controller.generateInsuranceQuote(person2), "Health Insurance Premium for Mr.Norman 6850" );

    }
}
