package com.emids.service;

import com.emids.entity.Person;
import com.emids.repo.QuoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QuoteService {
    @Autowired
    private QuoteRepository repository;
    private Person person;
    private long basePrice = 5000;

    private final Logger logger  = LoggerFactory.getLogger(QuoteService.class);

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public long getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(long basePrice) {
        this.basePrice = basePrice;
    }

    public long getQuote(Person person) {
        //person = new Person();
        repository.save(person);
        this.setPerson(person);
        logger.info("<<<<<<<>>>>>>>>"+person);
        return generateQuote();
    }

    public long generateQuote() {
        long quote;
        quote = this.getBasePrice();
     //   logger.info("Quote>>>>"+quote);
        quote = ageFactor();
        logger.info("Quote>>>>"+quote);
        quote = genderFactor(quote);
        logger.info("Quote>>>>"+quote);
        quote = hypertensionFactor(quote);
        logger.info("Quote>>>>"+quote);
        quote = bloodPressureFactor(quote);
        logger.info("Quote>>>>"+quote);
        quote =         bloodSugarFactor(quote);
        logger.info("Quote>>>>"+quote);
        quote =        overWeightFactor(quote);
        logger.info("Quote>>>>"+quote);
        quote =        alcoholicFactor(quote);
        logger.info("Quote>>>>"+quote);
        quote =        smokerFactor(quote);
        logger.info("Quote>>>>"+quote);
        quote =         drugAddictFactor(quote);
        logger.info("Quote>>>>"+quote);
        quote =        excerciseFactor(quote);
        logger.info("Quote>>>>"+quote);
        return quote;

    }

    private long ageFactor() {
       // logger.info("<<<<<<<<<Age of the customer is >>>>"+this.person.getAge());
        int ageFactorPercent = 40;
        long quote = this.getBasePrice();
        int age = this.person.getAge();
        if (age<18) {
            ageFactorPercent = 0;
        }  if (age >=18) {
            quote = Math.round(quote + quote* 0.1);
            //logger.info("Age FActor Quote1>>>>"+quote);
        }  if (age >=25) {
            quote = Math.round(quote + quote* 0.1);
            //logger.info("Age FActor Quote2>>>>"+quote);
        }  if (age >=30) {
            quote = Math.round(quote + quote* 0.1);
            //logger.info("Age FActor Quote3>>>>"+quote);
        }  if (age >=35) {
            quote = Math.round(quote + quote* 0.1);
            //logger.info("Age FActor Quote>>>>"+quote);
        } if (age>=40) {
            int ageCounter = 40;
            ageFactorPercent = 40;
            while (age>ageCounter) {
                ageCounter+=5;
                quote = Math.round(quote + quote* 0.2);
              //  logger.info("Age FActor Quote>>>>"+quote);
            }
        }
        logger.info("Age FActor Quote>>>>"+quote);
        return quote;
    }

    private long genderFactor(long quote) {
        if (this.person.getGender() == 'M') {
            logger.info("Male factor, increase it by 2%");
            return Math.round(quote + quote*0.02);
        } else {
            return Math.round(quote);
        }
    }

    private long hypertensionFactor(long quote) {
        if (this.person.isHasHypertension()) {
            return Math.round(quote + quote * 0.01);
        } else {
            return Math.round(quote);
        }
    }
    private long bloodPressureFactor(long quote) {
        if (this.person.isHasBP()) {
            return Math.round(quote + quote*0.01);
        } else {
            return Math.round(quote);
        }
    }

    private long bloodSugarFactor(long quote) {
        if (this.person.isHasBloodSugar()) {
            return Math.round(quote + quote * 0.01);
        } else {
            return Math.round(quote);
        }
    }

    private long overWeightFactor(long quote) {
        if (this.person.isOverWeight()) {
            return Math.round(quote + quote * 0.01);
        } else {
            return Math.round(quote);
        }
    }

    private long smokerFactor(long quote) {
        if (this.person.isSmoker()) {
            return Math.round(quote + quote * 0.03);
           // return Math.round(quote);
        }else {
            return Math.round(quote);
        }
    }

    private long alcoholicFactor(long quote) {
        if (this.person.isAlcholic()) {
            return Math.round(quote + quote * 0.03);
        } else {
            return Math.round(quote);
        }
    }

    private long drugAddictFactor(long quote) {
        if (this.person.isDrugAddict()) {
            return Math.round(quote + quote * 0.03);
        } else {
            return Math.round(quote);
        }
    }

    private long excerciseFactor(long quote) {
        if (this.person.isExcerciseFlag()) {
            return Math.round(quote -  quote * 0.03);
        } else {
            return Math.round(quote);
        }
    }
}
