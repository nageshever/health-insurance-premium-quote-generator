package com.emids;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InsuranceQuoteGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(InsuranceQuoteGeneratorApplication.class, args);
	}
}
