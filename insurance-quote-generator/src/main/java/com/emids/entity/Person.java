package com.emids.entity;

import org.springframework.context.annotation.Bean;

import javax.persistence.*;

@Entity
@Table (name = "person")
public class Person {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Long id;
    private String firstName;
    private String lastName;

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    private int age;
    private char gender;

    private boolean hasHypertension;
    private boolean hasBP;
    private boolean hasBloodSugar;
    private boolean isOverWeight;

    private boolean isSmoker;
    private boolean isAlcholic;
    private boolean excerciseFlag;
    private boolean isDrugAddict;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFristName(String fristName) {
        this.firstName = firstName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public boolean isHasHypertension() {
        return hasHypertension;
    }

    public void setHasHypertension(boolean hasHypertension) {
        this.hasHypertension = hasHypertension;
    }

    public boolean isHasBP() {
        return hasBP;
    }

    public void setHasBP(boolean hasBP) {
        this.hasBP = hasBP;
    }

    public boolean isHasBloodSugar() {
        return hasBloodSugar;
    }

    public void setHasBloodSugar(boolean hasBloodSugar) {
        this.hasBloodSugar = hasBloodSugar;
    }

    public boolean isOverWeight() {
        return isOverWeight;
    }

    public void setOverWeight(boolean overWeight) {
        isOverWeight = overWeight;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", fullName='" + firstName + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                ", hasHypertension=" + hasHypertension +
                ", hasBP=" + hasBP +
                ", hasBloodSugar=" + hasBloodSugar +
                ", isOverWeight=" + isOverWeight +
                ", isSmoker=" + isSmoker +
                ", isAlcholic=" + isAlcholic +
                ", excerciseFlag=" + excerciseFlag +
                ", isDrugAddict=" + isDrugAddict +
                '}';
    }

    public boolean isSmoker() {
        return isSmoker;
    }

    public void setSmoker(boolean smoker) {
        isSmoker = smoker;
    }

    public boolean isAlcholic() {
        return isAlcholic;
    }

    public void setAlcholic(boolean alcholic) {
        isAlcholic = alcholic;
    }

    public boolean isExcerciseFlag() {
        return excerciseFlag;
    }

    public void setExcerciseFlag(boolean excerciseFlag) {
        this.excerciseFlag = excerciseFlag;
    }

    public boolean isDrugAddict() {
        return isDrugAddict;
    }

    public void setDrugAddict(boolean drugAddict) {
        isDrugAddict = drugAddict;
    }

    public Person(String firstName, String lastName, int age, char gender, boolean hasHypertension, boolean hasBP, boolean hasBloodSugar,
                  boolean isOverWeight, boolean isSmoker, boolean isAlcholic, boolean excerciseFlag,
                  boolean isDrugAddict) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.gender = gender;
        this.hasHypertension = hasHypertension;
        this.hasBP = hasBP;
        this.hasBloodSugar = hasBloodSugar;
        this.isOverWeight = isOverWeight;
        this.isSmoker = isSmoker;
        this.isAlcholic = isAlcholic;
        this.excerciseFlag = excerciseFlag;
        this.isDrugAddict = isDrugAddict;
    }

    public Person() {
    }
}
