package com.emids;

import com.emids.entity.Person;
import com.emids.service.QuoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QuoteController {

    private final Logger logger = LoggerFactory.getLogger(QuoteController.class);

    @Autowired
    QuoteService service;

    @RequestMapping(value = "/quote",
    method = RequestMethod.POST,
    consumes = MediaType.APPLICATION_JSON_VALUE,
    produces = MediaType.APPLICATION_JSON_VALUE)
    public String generateInsuranceQuote(@RequestBody Person person) {
        logger.info(person.toString());
        String prefix = person.getGender() == 'M'?"Mr.":person.getGender() == 'F'?"Ms.":"";

        return "Health Insurance Premium for "+prefix+"" +person.getLastName() +" "
                +Math.round(service.getQuote(person));
    }

    @RequestMapping(value="/", method=RequestMethod.GET)
    public Person getPerson() {
        return new Person();

    }
}
