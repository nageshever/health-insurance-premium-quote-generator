package com.emids.repo;

import com.emids.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;

public interface QuoteRepository extends JpaRepository<Person, Long>
{
}
